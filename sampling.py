from collections import defaultdict, Counter

def get_article_concepts(articles, concepts, concept_weighting_scheme="idf"):
# ----------------------------------------------------
# ---  Prepare a set of concepts for each article  ---
    article_concepts = {}
    if concept_weighting_scheme == "ch":
        for v_id, article in articles.iteritems():
            article_concepts[v_id] = set(article["chosen_concepts"])
    else:
        # ---      General concepts will be ignored!!!      ---
        for v_id, article in articles.iteritems():
            article_concepts[v_id] = set([c for c in article["concepts"]
                                          if not concepts[c]["general"]])
    return article_concepts

def get_article_references(articles):
    article_references = defaultdict(set)
    for v_id, article in articles.iteritems():
        if "references" in article:
            article_references[v_id] = set(article["references"])
    return article_references

def get_authors(articles, articles_to_cluster=dict()):
    if not any(articles_to_cluster):
        articles_to_cluster = articles
    authors = set()
    for article in articles_to_cluster:
        for author in articles[article]["authors"]:
            authors.add(author)
    return list(authors)

def get_author_articles(author, articles, articles_to_cluster, arxiv_id=True):
    publications = set()
    for article_id in articles_to_cluster:
        if author in articles[article_id]["authors"]:
            if arxiv_id:
                publications.add(articles[article_id]["arxiv_id"])
            else:
                publications.add(article_id)
    return publications
