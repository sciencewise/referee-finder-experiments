# -*- coding: utf-8 -*-
# This script...

import os
import json
json.encoder.FLOAT_REPR = lambda o: format(o, '.6f')

import math
import random
import numpy as np
import matplotlib.pyplot as plt
import igraph
from collections import defaultdict, Counter, OrderedDict
from timeit import default_timer as timer
import itertools
import codecs

import metrics, network, sampling

data = "arxivPhys2013_wos"
incoming_data = "BC_A_wos"
concept_weighting_scheme = "tfidf"    # idf/tfidf
aggregation_scheme = "aggregated"   # concepts/references/aggregated/combined
layer = "combined"   # concepts/references/aggregated/combined
similarity_criterion = "cosine" # cosine/modularity

if aggregation_scheme == "references":
    description_title = data + ", " + aggregation_scheme + ", " + layer + ", " + similarity_criterion
else:
    description_title = data + ", " + aggregation_scheme + ", " + layer + ", " + concept_weighting_scheme + ", " + similarity_criterion

data_file = "data/" + data + ".json"
incoming_data_file = "data/" + incoming_data + ".json"
# partition_file = "partitions/" + data + "_" + concept_weighting_scheme + "_combined.json"
if aggregation_scheme == "references":
    partition_file = "partitions/" + data + "_references.json"
    visual_file = "visual/" + data + "_references.json"
else:
    partition_file = "partitions/" + data + "_" + "idf" + "_" + aggregation_scheme + ".json"    # concept_weighting_scheme -> "idf"
    visual_file = "visual/" + data + "_" + concept_weighting_scheme + "_" + aggregation_scheme + ".json"
# partition_file = "partitions/arxiv2010-2015_wos_combined_bp-uw_ref-idf-self.json"

# ----------------------------------------------------
# ---      Reading entire dataset (JSON file)      ---
if not os.path.exists(data_file):
    exit('\tError: json file {0} was not found!'.format(data_file))
entire_data = json.loads(open(data_file).read())

# ----------------------------------------------------
# ---      Reading entire incoming dataset (JSON file)      ---
if not os.path.exists(incoming_data_file):
    exit('\tError: json file {0} was not found!'.format(incoming_data_file))
incoming_data = json.loads(open(incoming_data_file).read())

# ----------------------------------------------------
# ---   Reading resulting hierarchical partition   ---
# print '\n', partition_file#, '\n'
if not os.path.exists(partition_file):
    exit('\tError: json file {0} was not found!'.format(partition_file))
hierarchical_partition = json.loads(open(partition_file).read())

# ----------------------------------------------------
# ---   Reading experts indices   ---
experts_file = 'report/experts_level1-5.json'
if not os.path.exists(experts_file):
    exit('\tError: json file {0} was not found!'.format(experts_file))
level_experts = json.loads(open(experts_file).read())

# print '\nSimilarity criterion:', similarity_criterion#, '\n'
print description_title

report_log = codecs.open("report/" + data + "_" + concept_weighting_scheme + "_" + aggregation_scheme + "_" + similarity_criterion + "_" + layer + "_level-3_experts_spec_BC.log", 'w', encoding="utf8")

report_json = codecs.open("report/" + data + "_" + concept_weighting_scheme + "_" + aggregation_scheme + "_" + similarity_criterion + "_" + layer + "_level-3_experts_spec_BC.json", 'w', encoding="utf8")

report = {}

# experts_json = codecs.open("report/experts2010-2015.json", 'w', encoding="utf8")

# ----------------------------------------------------
# ---       Prepare a hierarchy of clusters        ---
all_clusters = set()
root_clusters = set()
cluster_children = {}
cluster_parent = {}
for level_partition in hierarchical_partition["single_level_partitions"]:
    level = level_partition["level"]
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        all_clusters.add(cluster_id)
        if level == 1:
            root_clusters.add(cluster_id)
        parent_id = cluster["parent_id"]
        cluster_parent[cluster_id] = parent_id
        if not parent_id == 0:
            if parent_id not in cluster_children:
                cluster_children[parent_id] = set()
            cluster_children[parent_id].add(cluster_id)


def has_numbers(input_string):
    return any(char.isdigit() for char in input_string)

articles = entire_data["articles"]
incoming_articles = incoming_data["articles"]
print 'diff', len(set(articles) - set(incoming_articles)), len(articles), len(incoming_articles)
article_concepts = sampling.get_article_concepts(articles, entire_data["concepts"])
incoming_article_concepts = sampling.get_article_concepts(incoming_articles, incoming_data["concepts"])
concepts = set()
for article_id in articles:
    concepts |= article_concepts[article_id]
article_references = sampling.get_article_references(articles)

refs = set()
arts = set()
for art, ref in article_references.iteritems():
    refs |= ref
    arts.add(articles[art]["arxiv_id"])

false_articles = set()

# level_experts = dict()
descending_time = timer()
for current_article in incoming_articles:
    current_cluster = 0
    print "article {}: {}/{} {}".format(current_article, list(incoming_articles).index(current_article), len(incoming_articles), timer() - descending_time)
    descending_time = timer()
    for level in range(0, len(hierarchical_partition["single_level_partitions"])):
        # print "LEVEL", level
        if level > 2:
            continue

        # ----------------------------------------------------
        # ---       Sets of articles in each cluster       ---
        clusters_nodes = {}
        clusters = hierarchical_partition["single_level_partitions"][level]["clusters"]
        for cluster in clusters:
            cluster_id = cluster["cluster_id"]
            clusters_nodes[cluster_id] = cluster["nodes"]

        # ------------------------------------------------------
        # ---       Get list of idfs for each parent cluster       ---
        # ---       Get list of experts for each parent cluster       ---
        idfs = {}
        if 1+level != 1:
            experts = level_experts[str(1+level)]#{
        #     int(c): level_experts[str(1+level)][c]
        #     for c in level_experts[str(1+level)]
        # }

        if 1+level == 1:
            level_parents = set([cluster_parent[cluster_id] for cluster_id in clusters_nodes])
            # print 'p1', level_parents
        else:
            level_parents = set([current_cluster])
            # print 'px', level_parents
        for parent_cluster_id in level_parents:
            # if list(level_parents).index(parent_cluster_id) > 0:
            #     break
            # cluster_nodes = list(itertools.chain(*[clusters_nodes[cluster_id] for cluster_id in clusters_nodes if parent_cluster_id == cluster_parent[cluster_id]]))
            cluster_nodes = list()
            for cluster_id in clusters_nodes:
                if cluster_parent[cluster_id] == parent_cluster_id:
                    cluster_nodes += clusters_nodes[cluster_id]
            idfs[parent_cluster_id] = metrics.get_idf(article_concepts, cluster_nodes)
        #     print "Cluster {}/{}, experts ranking...".format(parent_cluster_id, len(level_parents))
        #     experts[parent_cluster_id] = network.get_experts(articles, cluster_nodes)
        #     print parent_cluster_id, len(cluster_nodes)
        # level_experts[1+level] = experts
        # continue

        # ------------------------------------------------------
        # ---       Get normalized average vector of each cluster       ---
        invalid_clusters = set()
        # print current_cluster, idfs.keys()
        cluster_vector, invalid_clusters, articles_vectors = metrics.get_average_cluster_vector(
            articles,
            idfs,
            current_cluster,
            clusters_nodes,
            clusters_nodes,
            article_concepts,
            weighting_scheme=concept_weighting_scheme)

        # print "__________________________________________________________________________________"
        # print "level\t|clusters|\t|vectorized|\t|invalid|\t|articles|\t|concepts|"
        # print "{:^5d}\t{:^10d}\t{:^12d}\t{:^10d}\t{:^10d}\t{:^10d}".format(
        #     1 + level,
        #     len(clusters_nodes),
        #     len(cluster_vector),
        #     len(invalid_clusters),
        #     len(articles_vectors),
        #     len(concepts))
        # print "----------------------------------------------------------------------------------\n"
        # report_log.write("level\t|clusters|\t|vectorized|\t|invalid|\t|articles|\t|concepts|\n")
        # report_log.write("{:^5d}\t{:^10d}\t{:^12d}\t{:^10d}\t{:^10d}\t{:^10d}\n".format(
            # 1 + level,
            # len(clusters_nodes),
            # len(cluster_vector),
            # len(invalid_clusters),
            # len(articles_vectors),
            # len(concepts)))

        # ------------------------------------------------------
        # Treat each parent cluster as a corpus
        level_clusters = list(set(cluster["cluster_id"] for cluster in clusters) - invalid_clusters)

        right_articles_to_level = 0
        articles_to_level = 0
        level_time = timer()
        for parent_cluster_id in list(level_parents):
            # if list(level_parents).index(parent_cluster_id) > 5:
            #     break
            right_probabilities = {}
            wrong_probabilities = {}
            right_articles = 0
            wrong_articles = 0
            articles_to_cluster = 0

            # ------------------------------------------------------
            # Treat each subcluster of parent cluster as cluster of corpus
            subclusters = [cluster_id for cluster_id in level_clusters if cluster_parent[cluster_id] == parent_cluster_id]
            # print subclusters, parent_cluster_id in [cluster_parent[cluster_id] for cluster_id in level_clusters]
            if len(subclusters) < 1:    # Cluster costains single subcluster
                continue
            # print 'after', subclusters
            cluster_nodes = list()
            for cluster_id in clusters_nodes:
                if cluster_parent[cluster_id] == parent_cluster_id:
                    cluster_nodes += clusters_nodes[cluster_id]

            # print "Parent cluster {} ({} subclusters):".format(parent_cluster_id, len(subclusters))
            report_log.write("Parent cluster {} ({} subclusters):\n".format(parent_cluster_id, len(subclusters)))

            subcluster_time = timer()
            if similarity_criterion == "modularity":
                cluster_network = network.get_N(
                    articles,
                    cluster_nodes,
                    article_concepts=article_concepts if layer in ["concepts", "combined"] else dict(),
                    article_references=article_references if layer in ["references", "combined"] else dict(),
                    article_tfs=articles if concept_weighting_scheme != "idf" or layer == "aggregated" else dict())

            for own_subcluster_id in [1]:#subclusters:
                # print 'own'
                own_clusters_nodes_correct = [1]#list(set(clusters_nodes[own_subcluster_id]) - false_articles)
                number_articles_to_subcluster = len(own_clusters_nodes_correct)
                #if number_articles_to_subcluster < 2:
                #     continue
                articles_to_cluster += number_articles_to_subcluster

                # ------------------------------------------------------
                # Get maximum similarity for each article in the selected cluster
                right_similarities = {}
                wrong_similarities = {}
                start = timer()
                if similarity_criterion == "cosine":
                    # print [current_article]
                    for article_id in [current_article]:#set(clusters_nodes[own_subcluster_id]) - false_articles:
                        similarity_time = timer()
                        cluster_vector, invalid_clusters, articles_vectors = metrics.get_average_cluster_vector(
                            articles,
                            idfs,
                            current_cluster,#cluster_parent,
                            clusters_nodes,
                            subclusters,
                            article_concepts,
                            article_id,
                            weighting_scheme=concept_weighting_scheme)
                        # article_vector = articles_vectors[article_id]
                        article_vector = metrics.get_normed_vector(
                            incoming_article_concepts[article_id],
                            idfs[current_cluster],
                            incoming_articles[article_id] if concept_weighting_scheme == "tfidf" else dict()
                            )
                        cosines = [metrics.get_similarity(article_vector, cluster_vector[cluster]) for cluster in subclusters]
                        lapse = timer() - similarity_time
                        idx_max = np.argmax(cosines)
                        own_subcluster_id = subclusters[idx_max]
                        # report_article = "{:>3d}/{}->{:<3d} {:>2d}->{:<2d} time {:5.4f}".format(
                        #     1 + own_clusters_nodes_correct.index(article_id),
                        #     len(own_clusters_nodes_correct),
                        #     len(set(clusters_nodes[subclusters[idx_max]]) - false_articles),
                        #     own_subcluster_id,
                        #     subclusters[idx_max],
                        #     lapse)
                        # if lapse > 1:
                        #     print report_article

                        # Show an expert

                        # print "Expert for", articles[article_id]["arxiv_id"], "is", network.get_expert(
                        #     articles,
                        #     articles_to_cluster=clusters_nodes[own_subcluster_id],
                        #     authors=articles[article_id]["authors"],
                        #     experts=experts[parent_cluster_id])
                        # report_log.write(report_article + '\n')
                        current_cluster = subclusters[idx_max]
                        if 1+level == 4:#own_subcluster_id == subclusters[idx_max]:
                            right_similarities[article_id] = cosines

                            cluster_articles = clusters_nodes[own_subcluster_id]
                            cluster_authors = sampling.get_authors(articles, cluster_articles)
                            author_articles = {}
                            for author in cluster_authors:
                                author_articles[author] = sampling.get_author_articles(author, articles, cluster_articles, arxiv_id=False)
                            # Show an author
                            cluster_articles = set(clusters_nodes[own_subcluster_id]) - false_articles
                            # author = network.get_guess_author(articles, cluster_articles, articles_vectors, article_vector)
                            auths = 0
                            specialists, auths = network.get_specialist(articles, cluster_articles, articles_vectors, article_id, incoming_articles[article_id]["authors"], cluster_authors, author_articles, article_vector)
                            # report_referee = u"{} for {} is {} of {} with {} as specialist".format("+" if author in articles[article_id]["authors"] else "-", articles[article_id]["authors"], author, articles[article_id]["arxiv_id"], specialists[0])
                            # print report_referee
                            # report_log.write(report_referee + '\n')
                            report[article_id] = {
                                "into": article_id in articles,
                                "arxiv_id": incoming_articles[article_id]["arxiv_id"],
                                "title": incoming_articles[article_id]["title"],
                                "authors": incoming_articles[article_id]["authors"],
                                "experts": network.get_ranked_experts(
                                    authors=incoming_articles[article_id]["authors"],
                                    experts=experts[str(parent_cluster_id)]),
                                "specialists": specialists,
                                # "experts": network.get_expert(
                                #     articles,
                                #     articles_to_cluster=clusters_nodes[own_subcluster_id],
                                #     authors=articles[article_id]["authors"],
                                #     experts=experts[parent_cluster_id]),
                                # "author": author,
                            }
                            print "a {}/{}:{} s {}/{} c {}/{} {:5.4f}".format(list(incoming_articles).index(article_id), len(incoming_articles), auths, 1 + subclusters.index(own_subcluster_id), len(subclusters), 1 + list(level_parents).index(parent_cluster_id), len(level_parents), (timer() - similarity_time))
                            # print "a {}/{}:{} s {}/{} c {}/{} {:5.4f}".format(list(set(clusters_nodes[own_subcluster_id]) - false_articles).index(article_id), len(set(clusters_nodes[own_subcluster_id]) - false_articles), auths, 1 + subclusters.index(own_subcluster_id), len(subclusters), 1 + list(level_parents).index(parent_cluster_id), len(level_parents), (timer() - similarity_time))
                        else:
                            pass
                            # print false_articles, false_articles & set(clusters_nodes[own_subcluster_id])
                            false_articles.add(article_id)
                            # print false_articles, len(set(clusters_nodes[own_subcluster_id]) - false_articles), len(clusters_nodes[own_subcluster_id])
                            wrong_similarities[article_id] = cosines
                else:   # elif similarity_criterion == "modularity":
                    for article_id in set(clusters_nodes[own_subcluster_id]) - false_articles:
                        similarity_time = timer()
                        # if concept_weighting_scheme == "tfidf":
                        #     cluster_network = network.get_N(cluster_nodes, article_concepts, article_references, articles=articles, new_article=articles[article_id]["arxiv_id"], aggregation_scheme=aggregation_scheme)
                        # else:
                        #     cluster_network = network.get_N(cluster_nodes, article_concepts, article_references, new_article=articles[article_id]["arxiv_id"], aggregation_scheme=aggregation_scheme)
                        modularities = [network.get_cluster_modularity(cluster_network, clusters_nodes[subcluster], article_id) for subcluster in subclusters]
                        lapse = timer() - similarity_time
                        idx_max = np.argmax(modularities)
                        report_article = "{:>3d}/{}->{:<3d} {:>2d}->{:<2d} time {:5.4f}".format(
                            1 + own_clusters_nodes_correct.index(article_id),
                            len(own_clusters_nodes_correct),
                            len(set(clusters_nodes[subclusters[idx_max]]) - false_articles),
                            own_subcluster_id,
                            subclusters[idx_max],
                            lapse)
                        if lapse > 1:
                            print report_article
                        print "Expert for", articles[article_id]["arxiv_id"], "is", network.get_expert(
                            articles,
                            articles_to_cluster=clusters_nodes[own_subcluster_id],
                            authors=articles[article_id]["authors"],
                            experts=experts[parent_cluster_id])
                        report_log.write(report_article + '\n')
                        if own_subcluster_id == subclusters[idx_max]:
                            right_similarities[article_id] = modularities
                            specialists = [(author, (network.get_cluster_modularity(cluster_network, author_articles[author], article_id), len(author_articles[author]), len(cluster_authors))) for author in cluster_authors]
                            specialists = OrderedDict(sorted(specialists, key=lambda s: s[1], reverse=False)[-5:])
                            report[article_id] = {
                                "arxiv_id": articles[article_id]["arxiv_id"],
                                "title": articles[article_id]["title"],
                                "authors": articles[article_id]["authors"],
                                "experts": get_ranked_experts(
                                    authors=articles[article_id]["authors"],
                                    experts=experts[parent_cluster_id]),
                                "specialists": specialists,
                            }
                            print "a {}/{}:{} s {}/{} c {}/{} {:5.4f}".format(list(set(clusters_nodes[own_subcluster_id]) - false_articles).index(article_id), len(set(clusters_nodes[own_subcluster_id]) - false_articles), auths, 1 + subclusters.index(own_subcluster_id), len(subclusters), 1 + list(level_parents).index(parent_cluster_id), len(level_parents), (timer() - similarity_time))
                        else:
                            false_articles.add(article_id)
                            wrong_similarities[article_id] = modularities
                # report_subcluster = "{:>5.1f}% - {:>3d}/{:<3d} correct articles in the cluster {:<3d} - {:>3d}:{}\t| time {:8.4f}".format(
                #     100. * len(right_similarities) / number_articles_to_subcluster,
                #     len(right_similarities),
                #     number_articles_to_subcluster,
                #     own_subcluster_id,
                #     1 + level_clusters.index(own_subcluster_id),
                #     len(level_clusters),
                #     timer() - start)
                # if timer() - start > 1:
                #     print report_subcluster
                # report_log.write(report_subcluster + '\n')
                right_probabilities[own_subcluster_id] = float(len(right_similarities))/number_articles_to_subcluster
                wrong_probabilities[own_subcluster_id] = float(len(wrong_similarities))/number_articles_to_subcluster
                right_articles += len(right_similarities)
                wrong_articles += len(wrong_similarities)
            # print "__________________________________"
            report_cluster = "Fraction of successful assignments {:>4d}/{:<4d} - {:5.1f}%\t| time {:8.4f}\n".format(
                right_articles,
                articles_to_cluster,
                100 * float(right_articles) / (articles_to_cluster if articles_to_cluster else 1),
                timer() - subcluster_time)
            # if timer() - subcluster_time > 1:
            #     print report_cluster
            # report_log.write(report_cluster + '\n')
            right_articles_to_level += right_articles
            articles_to_level += articles_to_cluster
        report_level = "Success rate of Level {}: {}/{} - {:5.1f}%\t| time {:8.4f}\n".format(
            level + 1,
            right_articles_to_level,
            articles_to_level,
            100 * float(right_articles_to_level) / (articles_to_level|1),
            timer() - level_time)
        # print report_level
        # report_log.write(report_level + '\n')

# experts_json.write(json.dumps(level_experts, ensure_ascii=False, indent=2))
report_json.write(json.dumps(report, ensure_ascii=False, indent=2))
