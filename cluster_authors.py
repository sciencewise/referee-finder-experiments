
exit()

# ------------------------------------------------------
# Plot and print wrong and right similarities
if any(wrong_similarities):
    article_id = wrong_similarities.keys()[0]
    article_vector = metrics.get_normed_vector(
        article_concepts[article_id],
        cluster_idfs,
        articles[article_id]["concepts"] if concept_weighting_scheme == "tfidf" else None
        )
    origin_similarity = metrics.get_similarity(article_vector, cluster_vector[own_cluster_id])
        
    fig, ax = plt.subplots()
    ax.spines['left'].set_position(('outward', 5))
    ax.spines['bottom'].set_position(('outward', 5))
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    plt.xlabel('Probability of right~wrong assignment')
    plt.ylabel('Number of clusters')
    # plt.xlabel('Cosine similarity of article~cluster')
    # plt.ylabel('Number of clusters with certain cosine similarity')

    # ax.hist(wrong_probabilities.values(), 100, bottom=0, log=1, color="red", alpha=0.5)
    ax.hist(right_probabilities.values(), 10, bottom=0, log=0, color="blue", alpha=0.5)
    # ax.hist(wrong_similarities[article_id], 100, bottom=1, log=1, color="red", alpha=0.5)
    # ax.annotate('true similarity\n' + str(article_id) + '~' + str(own_cluster_id),
    #     xy=(origin_similarity, 1),
    #     xytext=(origin_similarity-0.05, 10),
    #     arrowprops=dict(facecolor='black', shrink=0.05),
    #     horizontalalignment='right',
    #     verticalalignment='top')

    # for article in wrong_similarities[:5]:
    print "Similarities:\nWrong\tRight\n{:6.4f}\t{:6.12f}\n".format(max(wrong_similarities[article_id]), origin_similarity)
    idx_max = np.argmax(wrong_similarities[article_id])
    print "IDs:\n{}\t{}\n".format(level_clusters[idx_max], own_cluster_id)
    print "Clusters similarity:\n{:06.4f}\n".format(metrics.get_similarity(cluster_vector[level_clusters[idx_max]], cluster_vector[own_cluster_id]))
    plt.show()

c_id = 11
cluster_authors = {}
for cluster_id, articles in cluster_nodes.iteritems():
    if not cluster_id == c_id:
        continue
    cluster_authors[cluster_id] = {}
    for v_id in articles:
        # print '----------------------------\n', entire_data["articles"][v_id]
        a_id = entire_data["articles"][v_id]["arxiv_id"]
        a_title = entire_data["articles"][v_id]["title"].replace("\n", "")
        a_authors = entire_data["articles"][v_id]["authors"]
        number_authors = len(a_authors)
        # print "\t", a_id, "\t", a_title
        # print "\t\t", a_authors
        for author in a_authors:
            # if author == "_O":
            #    print a_id, "\t", a_authors
            if has_numbers(author):
                continue
            if author not in cluster_authors[cluster_id]:
                cluster_authors[cluster_id][author] = 0
            cluster_authors[cluster_id][author] += 1.0 / number_authors
        # print "\t\t", author

cluster_id = c_id
# for author, n_articles in cluster_authors[cluster_id].iteritems():
#    print author, n_articles

for author in sorted(cluster_authors[cluster_id], key=cluster_authors[cluster_id].__getitem__, reverse=True)[:20]:
    print author, "\t", cluster_authors[cluster_id][author]

