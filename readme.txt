Script "get_partition.py" creates a hierarchical partition that is recorded as a JSON file within "partitions" directory.

Properties of hierarchical partition json file:
1. It is a dictionary that have a single key: "single_level_partitions"
2. The value of this key is a list of single level partitions.
3. Each single level partition is represented as a dict that contain:
    - "level" : id of hierarchical level (1 is the first level, 2 - the next level in hierarchy)
    - "clusters" : a list of all clusters of a given level partition.
    - each cluster is represented by a dict that contain the following information about the cluster:
	  - "cluster_id" : its id
	  - "parent_id"  : id of another cluster that is considered as a parent of the given one
	  - "parent_id" equals to 0 correspond to the entire collection of articles
	  - "nodes" : a list of nodes that belong to the cluster
4. The partitions of each level contain the same nodes.
5. If a cluster has 1 node only, it can not be divided recursively, however the node will appear in all subsequent level partitions.
   Thus, a cluster with a single node may appear in several levels of hierarchy with the same "cluster_id", "parent_id" and "nodes".


Script "visualize_partition.py" reads the resulting partition and prepares another JSON file (within "visual" directory)
that will be used for visualization.
