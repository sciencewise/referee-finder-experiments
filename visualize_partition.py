# This script...

import os
import json
import math

data = "BC_A_restricted"
concept_weighting_scheme = "tfidf"
aggregation_scheme = "combined"

if aggregation_scheme == "references":
    description_title = data + ", " + aggregation_scheme
else:
    description_title = data + ", " + aggregation_scheme + ", " + concept_weighting_scheme

data_file = "data/" + data + ".json"
if aggregation_scheme == "references":
    partition_file = "partitions/" + data + "_references.json"
    visual_file = "visual/" + data + "_references.json"
else:
    partition_file = "partitions/" + data + "_" + concept_weighting_scheme + "_" + aggregation_scheme + ".json"
    visual_file = "visual/" + data + "_" + concept_weighting_scheme + "_" + aggregation_scheme + ".json"


# ----------------------------------------------------
# ---      Reading entire dataset (JSON file)      ---
if not os.path.exists(data_file):
    exit('\tError: json file {0} was not found!'.format(data_file))
entire_data = json.loads(open(data_file).read())

# ----------------------------------------------------
# ---   Reading resulting hierarchical partition   ---
if not os.path.exists(partition_file):
    exit('\tError: json file {0} was not found!'.format(partition_file))
hierarchical_partition = json.loads(open(partition_file).read())


# ----------------------------------------------------
# ---     Prepare resulting JSON to visualize      ---
vis_json = {
    "title": description_title,
    "name": "All",
    "children": []
}


# ----------------------------------------------------
# ---       Prepare a hierarchy of clusters        ---
all_clusters = set()
root_clusters = set()
cluster_children = {}
cluster_parent = {}
for level_partition in hierarchical_partition["single_level_partitions"]:
    level = level_partition["level"]
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        all_clusters.add(cluster_id)
        if level == 1:
            root_clusters.add(cluster_id)
        parent_id = cluster["parent_id"]
        cluster_parent[cluster_id] = parent_id
        if not parent_id == 0:
            if parent_id not in cluster_children:
                cluster_children[parent_id] = set()
            cluster_children[parent_id].add(cluster_id)

# ----------------------------------------------------
# ---        Sets of nodes in each cluster         ---
cluster_nodes = {}
for level_partition in hierarchical_partition["single_level_partitions"]:
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        cluster_nodes[cluster_id] = cluster["nodes"]

# ----------------------------------------------------
# --- Calculate concept DF, TF and IDF within each cluster ---
cluster_concept_df = {}
cluster_concept_tf = {}
cluster_concept_idf = {}

for level_partition in hierarchical_partition["single_level_partitions"]:
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        this_cluster_nodes = cluster["nodes"]

        cluster_concept_df[cluster_id] = {}
        cluster_concept_tf[cluster_id] = {}
        for v_id in this_cluster_nodes:
            for c in entire_data["articles"][v_id]["concepts"]:
                if entire_data["concepts"][c]["general"]:
                    continue
                # Calculate concept df in the cluster
                if c not in cluster_concept_df[cluster_id]:
                    cluster_concept_df[cluster_id][c] = 0
                cluster_concept_df[cluster_id][c] += 1
                # Calculate concept tf in the cluster
                if c not in cluster_concept_tf[cluster_id]:
                    cluster_concept_tf[cluster_id][c] = 0
                c_tf = entire_data["articles"][v_id]["concepts"][c]
                cluster_concept_tf[cluster_id][c] += c_tf
        # Calculate IDF for each concept within the cluster
        # Note! To calulate TFIDF one have to multiply TF from given cluster and IDF from a parent cluster
        cluster_concept_idf[cluster_id] = {}
        for c, c_df in cluster_concept_df[cluster_id].iteritems():
            c_idf = math.log10(1.0 * len(this_cluster_nodes) / c_df)
            cluster_concept_idf[cluster_id][c] = c_idf

cluster_concept_idf[0] = {}  # IDF for the entire collection
entire_concept_df = {}
entire_concept_tf = {}
for v_id in entire_data["articles"]:
    for c in entire_data["articles"][v_id]["concepts"]:
        if entire_data["concepts"][c]["general"]:
            continue
        if c not in entire_concept_df:
            entire_concept_df[c] = 0
        if c not in entire_concept_tf:
            entire_concept_tf[c] = 0
        entire_concept_df[c] += 1
        c_tf = entire_data["articles"][v_id]["concepts"][c]
        entire_concept_tf[c] += c_tf
for c, c_df in entire_concept_df.iteritems():
    c_idf = math.log10(1.0 * len(entire_data["articles"]) / c_df)
    cluster_concept_idf[0][c] = c_idf

# ----------------------------------------------------
# --- Calculate concept DFIDF and TFIDF within each cluster ---
cluster_concept_dfidf = {}
cluster_concept_tfidf = {}
for level_partition in hierarchical_partition["single_level_partitions"]:
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        parent_id = cluster["parent_id"]

        cluster_concept_dfidf[cluster_id] = {}
        for c, c_df in cluster_concept_df[cluster_id].iteritems():
            c_idf = cluster_concept_idf[parent_id][c]
            cluster_concept_dfidf[cluster_id][c] = c_df * c_idf

        cluster_concept_tfidf[cluster_id] = {}
        for c, c_tf in cluster_concept_tf[cluster_id].iteritems():
            c_idf = cluster_concept_idf[parent_id][c]
            cluster_concept_tfidf[cluster_id][c] = c_tf * c_idf


# ---------------------------------------------------------------------------------------
level_clusters = {}
max_level = 1
for level_partition in hierarchical_partition["single_level_partitions"]:
    level = level_partition["level"]
    if level > max_level:
        max_level = level
    if level not in level_clusters:
        level_clusters[level] = set()
    for cluster in level_partition["clusters"]:
        cluster_id = cluster["cluster_id"]
        level_clusters[level].add(cluster_id)

cluster_description = {}
for cluster_id in all_clusters:
    cluster_description[cluster_id] = {}
    if cluster_concept_tfidf[cluster_id]:
        concept_id = \
            sorted(cluster_concept_tfidf[cluster_id], key=cluster_concept_tfidf[cluster_id].__getitem__, reverse=True)[0]
        name = str(cluster_id) + " " + entire_data["concepts"][concept_id]["name"]
    else:
        name = "None"
    cluster_description[cluster_id]["name"] = name

    cluster_description[cluster_id]["concepts_tfidf"] = {}
    rank = 1
    for c in sorted(cluster_concept_tfidf[cluster_id], key=cluster_concept_tfidf[cluster_id].__getitem__, reverse=True)[:20]:
        c_name = entire_data["concepts"][c]["name"]
        cluster_description[cluster_id]["concepts_tfidf"][rank] = c_name
        rank += 1

    """
    cluster_description[cluster_id]["concepts_dfidf"] = {}
    rank = 1
    for c in sorted(cluster_concept_dfidf[cluster_id], key=cluster_concept_dfidf[cluster_id].__getitem__, reverse=True)[:20]:
        c_name = entire_data["concepts"][c]["name"]
        cluster_description[cluster_id]["concepts_dfidf"][rank] = c_name
        rank += 1
    """

    # Rank cluster articles
    article_number_concepts = {}
    for v_id in cluster_nodes[cluster_id]:
        n_concepts = len(entire_data["articles"][v_id]["concepts"])
        article_number_concepts[v_id] = n_concepts

    cluster_description[cluster_id]["articles"] = {}
    rank = 1
    for v_id in sorted(article_number_concepts, key=article_number_concepts.__getitem__, reverse=True)[:20]:
        a_id = entire_data["articles"][v_id]["article_id"]
        a_title = entire_data["articles"][v_id]["title"]
        a_authors = entire_data["articles"][v_id]["authors"]
        a_pc = entire_data["articles"][v_id]["primary_category"]
        cluster_description[cluster_id]["articles"][rank] = {
            "arxiv_id": a_id,
            "title": a_title,
            "authors": a_authors,
            "category": a_pc
        }
        rank += 1

    if cluster_id not in cluster_children:
        number_articles = len(cluster_nodes[cluster_id])
        cluster_description[cluster_id]["children"] = [
            {"name": name, "value": number_articles}
        ]
        # cluster_description[cluster_id]["value"] = number_articles

children = {}
for level in range(max_level, 0, -1):
    for cluster_id in level_clusters[level]:
        if level - 1 in level_clusters:
            if cluster_id in level_clusters[level - 1]:
                continue
        parent_id = cluster_parent[cluster_id]
        if cluster_id in children:
            cluster_description[cluster_id]["children"] = children[cluster_id]

        if parent_id not in children:
            children[parent_id] = []
        children[parent_id].append(cluster_description[cluster_id])

# print children[0]

vis_json["children"] = children[0]

# Top concepts in the entire collection
vis_json["concepts_tfidf"] = {}
rank = 1
for c in sorted(entire_concept_tf, key=entire_concept_tf.__getitem__, reverse=True)[:20]:
    c_name = entire_data["concepts"][c]["name"]
    vis_json["concepts_tfidf"][rank] = c_name
    rank += 1

"""
vis_json["concepts_dfidf"] = {}
rank = 1
for c in sorted(entire_concept_df, key=entire_concept_df.__getitem__, reverse=True)[:20]:
    c_name = entire_data["concepts"][c]["name"]
    vis_json["concepts_dfidf"][rank] = c_name
    rank += 1
"""

with open(visual_file, 'wb') as f:
    f.write(json.dumps(vis_json, ensure_ascii=False, indent=1).encode('utf8'))
