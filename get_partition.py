"""
This script:
 1) reads original data file that contains both concepts and references for each article,
 2) represents the data as two layer network and considers the aggregated network from two layers,
 3) apply Louvain (python-igraph) algorithm to identify community structure,
 4) performs recursive analysis on each cluster (with n_nodes > 1) to obtain hierarchy.
The number of hierarchical levels should be specified in advance.
"""

import os
import json
import math
import igraph
import itertools

import metrics, network, sampling
from timeit import default_timer as timer

from collections import defaultdict, Counter

concept_weighting_scheme = "idf"
"""
Possible weighting schemes for concepts:
 - ch;   # chosen_concepts
 - idf;
 - tfidf.
"""

aggregation_scheme = "combined"
"""
Possible aggregation schemes for concepts and references:
 - concepts;   # only concept similarity
 - references; # only bibliographic coupling
 - aggregated; # simple aggregation
 - combined.   # 50% of total link weight comes from concepts, 50% - from references.
"""

data = "BC_A_wos"
data_file = "data/" + data + ".json"
if aggregation_scheme == "references":
    partition_file = "partitions/{}_references.json".format(data)
else:
    partition_file = "partitions/-{}_{}_{}.json".format(data, concept_weighting_scheme,
                                                       aggregation_scheme)

min_number_nodes_to_cluster = 10  # should be 2 or higher
number_hierarchical_levels = 5  # al least 1 (no hierarchy)

# ----------------------------------------------------
# ---      Reading entire dataset (JSON file)      ---
if not os.path.exists(data_file):
    exit('\tError: json file {0} was not found!'.format(data_file))
entire_data_all = json.loads(open(data_file).read())

print "Number of articles:", len(entire_data_all["articles"])
print "Last article:", entire_data_all["articles"].keys()[-1]
for new_article in ["all"]:#entire_data_all["articles"].keys() + ["all"]:
    # print entire_data_all["articles"].keys().index(new_article)
    entire_data = {
        "articles": {
            art_id: entire_data_all["articles"][art_id]
            for art_id in set(entire_data_all["articles"].keys()) - set(new_article)
        },
        "concepts": entire_data_all["concepts"]
    }

    articles = entire_data["articles"]
    # ----------------------------------------------------
    # ---  Prepare a set of concepts for each article  ---
    article_concepts = sampling.get_article_concepts(articles, entire_data["concepts"])
    concepts = set()
    for article_id in articles:
        concepts |= article_concepts[article_id]

    # ----------------------------------------------------
    # ---  Prepare a set of references of each article ---
    article_references = sampling.get_article_references(articles)

    # ----------------------------------------------------
    # ---  Initial (entire) collection (cluster_id = 0) --
    level_id = 0
    cluster_id = 0
    current_level_partition = {
        "level": level_id,
        "clusters": [
            {
                "cluster_id": cluster_id,
                "nodes": list(articles.keys())
            }
        ]
    }


    # ----------------------------------------------------
    # ---        Prepare hierarchical partition        ---
    hierarchical_partition = {
        new_article: {"single_level_partitions": []}
    }
    # ----------------------------------------------------------------------------
    # --- Prepare next level partition based on the partition of a given level ---
    while level_id < number_hierarchical_levels:
        print "\n---- preparing partition of level {0} ----".format(level_id + 1)
        next_level_partition = {
            "level": level_id + 1,
            "clusters": []
        }
        for cluster in current_level_partition["clusters"]:
            # Here cluster is an object that contain "cluster_id" and "nodes"
            articles_to_cluster = cluster["nodes"]
            number_articles_to_cluster = len(articles_to_cluster)
            # print "\tNumber nodes to cluster: {0}".format(number_articles_to_cluster)
            if number_articles_to_cluster < min_number_nodes_to_cluster:
                next_level_partition["clusters"].append(cluster)
                continue

            # ----------------------------------------------------
            # ---   Represent specified cluster as a network   ---
            g = network.get_N(
                articles,
                articles_to_cluster,
                article_concepts=article_concepts if aggregation_scheme in ["concepts", "combined"] else dict(),
                article_references=article_references if aggregation_scheme in ["references", "combined"] else dict(),
                article_tfs=articles if concept_weighting_scheme != "idf" or aggregation_scheme == "aggregated" else dict())

            # ----------------------------------------------------
            # ---     Perform community detection analysis     ---
            partition = igraph.Graph.community_multilevel(g, weights=g.es["weight"], return_levels=False)
            # print "\tModularity (aggr) = {0:.3f}".format(g.modularity(partition, weights=weights))

            for new_cluster in partition:
                cluster_id += 1
                parent_id = cluster["cluster_id"]
                new_cluster_nodes = []
                for o_id in new_cluster:
                    v_id = g.vs["article_id"][o_id]
                    new_cluster_nodes.append(v_id)

                new_cluster_to_append = {
                    "cluster_id": cluster_id,
                    "parent_id": parent_id,
                    "nodes": new_cluster_nodes
                }
                # print "\t\t", cluster_id, parent_id, len(new_cluster_nodes)
                next_level_partition["clusters"].append(new_cluster_to_append)

        hierarchical_partition[new_article]["single_level_partitions"].append(next_level_partition)

        level_id += 1
        current_level_partition = next_level_partition

with open(partition_file, 'wb') as f:
    f.write(json.dumps(hierarchical_partition, ensure_ascii=False, indent=2).encode('utf8'))

