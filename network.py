# -*- coding: utf-8 -*-
import igraph
from collections import Counter, OrderedDict, defaultdict
import itertools
import numpy as np
import metrics, network, sampling
from timeit import default_timer as timer


def get_specialist(articles, cluster_articles, articles_vectors, article_id, article_authors, authors=None, author_articles=None, article_vector=None):
    if not authors:
        authors = sampling.get_authors(articles, cluster_articles)
    author_vector = defaultdict()
    authors_articles = {}
    for author in authors:
        # author_articles = sampling.get_author_articles(author, articles, cluster_articles, arxiv_id=False)
        net_author_articles = author_articles[author] - set([article_id])
        authors_articles[author] = len(net_author_articles)
        # if authors_articles[author] > 2:
        author_vector[author] = metrics.get_average_author_vector(net_author_articles, articles_vectors)
        # else:
        #     author_vector[author] = defaultdict()
    specialists = [(author, (metrics.get_similarity(article_vector, author_vector[author]), authors_articles[author], len(authors))) for author in authors]
    # specialists = [(author, (metrics.get_similarity(articles_vectors[article_id], author_vector[author]), authors_articles[author], len(authors))) for author in authors]
    specialists = OrderedDict(sorted(specialists, key=lambda s: s[1], reverse=False)[-5:])
    # specialist = specialists.popitem()
    # while specialist[0] in article_authors:
    #     try:
    #         specialist = specialists.popitem()
    #     except KeyError:
    #         return "nobody"
    return specialists, len(authors)


def get_guess_author(articles, cluster_articles, articles_vectors, article_vector):
    authors = sampling.get_authors(articles, cluster_articles)
    author_vector = {}
    for author in authors:
        author_articles = sampling.get_author_articles(author, articles, cluster_articles, arxiv_id=False)
        author_vector[author] = metrics.get_average_author_vector(author_articles, articles_vectors)
    author_similarities = [metrics.get_similarity(article_vector, author_vector[author]) for author in authors]
    author_max = np.argmax(author_similarities)
    return authors[author_max]


def get_ranked_experts(articles=defaultdict(), articles_to_cluster=dict(), authors=dict(), experts=dict()):
    if not any(experts):
        experts = get_experts(articles, articles_to_cluster)
    ranked_experts = OrderedDict()
    experts = OrderedDict(sorted(experts.items(), key=lambda h: h[1][0], reverse=True))
    h_groups = itertools.groupby(experts.items(), lambda h: h[1][0])
    experts = OrderedDict(itertools.chain.from_iterable([sorted(list(g), key=lambda h: h[1][1], reverse=True) for k, g in h_groups]))
    for expert, h_index in experts.iteritems():
        if expert not in authors:
            ranked_experts.update({expert: h_index})
        if len(ranked_experts) == 5:
            break
    return ranked_experts


def get_expert(articles, articles_to_cluster=dict(), authors=dict(), experts=dict()):
    if not any(experts):
        experts = get_experts(articles, articles_to_cluster)
    for expert in experts:
        if expert not in authors:
            return expert
    return "nobody"


def get_experts(articles, articles_to_cluster=dict()):
    experts = OrderedDict()
    start = timer()
    authors = list(sampling.get_authors(articles, articles_to_cluster))
    print 'auth', timer() - start
    authors_n = len(authors)
    for author in authors:
        start = timer()
        h_c = network.get_citation_index(author, articles)
        print 'cit', timer() - start
        start = timer()
        if h_c != (0, 0):
            experts.update({author: h_c})
        print "{}/{} h {} c {} {}".format(authors.index(author), authors_n, h_c[0], h_c[1], author.encode('utf-8', 'ignore'))
        print 'upd', timer() - start
        start = timer()
    # Sort by h-index
    h_index = Counter(experts.values())
    experts = OrderedDict(sorted(experts.items(), key=lambda h: h[1][0], reverse=True))

    # Sort by cites
    h_groups = itertools.groupby(experts.items(), lambda h: h[1][0])
    return OrderedDict(itertools.chain.from_iterable([sorted(list(g), key=lambda h: h[1][1], reverse=True) for k, g in h_groups]))
    # return experts


def get_citation_index(author, articles, articles_to_cluster=dict(), h_index=False):
    if not any(articles_to_cluster):
        articles_to_cluster = articles
    citations = Counter()
    publications = sampling.get_author_articles(author, articles, articles_to_cluster)
    for article_id in articles_to_cluster:
        citations.update(publications & set(articles[article_id]["references"]))
    if any(citations):
        rank_citations = sorted(citations.values(), reverse=True)
        return max(min(cits, index + 1) for index, cits in enumerate(rank_citations)), sum(citations.values())
    else:
        return 0, 0


def get_cluster_modularity(N, cluster_nodes_article_id, new_article, N_without_i=None):
    # N_without_i = N.subgraph(N.vs.select(article_id_ne=new_article))
    # S_N = sum(N_without_i.strength(N_without_i.vs, weights="weight"))
    # S_k = sum(N_without_i.strength(N_without_i.vs.select(article_id_in=cluster_nodes_article_id), weights="weight"))
    # W_ik = sum(N.es.select(_between=(N.vs.select(article_id=new_article), N.vs.select(article_id_in=cluster_nodes_article_id)))["weight"])

    k_with_i = N.subgraph(N.vs.select(article_id_in=cluster_nodes_article_id+[new_article]))    # subgraph ({i} + cluster k) of network N
    W_ik = k_with_i.strength(k_with_i.vs.select(article_id=new_article), weights="weight")[0]   # strength of node i over ({i} + cluster k)
    s_i = N.strength(N.vs.select(article_id=new_article), weights="weight")[0]    # strength of node i over network N
    S_k = sum(N.strength(N.vs.select(article_id_in=cluster_nodes_article_id), weights="weight")) - (W_ik if new_article not in cluster_nodes_article_id else W_ik + s_i)  # net strength of cluster k
    S_N = sum(N.strength(N.vs, weights="weight")) - s_i # net strength of network N
    try:
        hatW_ik = s_i * S_k / S_N   # expected link weight (strength) between node i and cluster k
    except ZeroDivisionError:
        hatW_ik = float("inf")
    return W_ik - hatW_ik


def get_N(articles, articles_to_cluster, article_concepts=dict(), article_references=dict(), article_tfs=dict(), new_article=None):
    # ----------------------------------------------------
    # ---              Concept similarity              ---
    # ----------------------------------------------------
    # ---     Calculate idf values of the concepts     ---
    concept_idf = metrics.get_idf(article_concepts, articles_to_cluster)
    # ----------------------------------------------------
    # --- Bibliographic coupling (reference similarity) --
    # ----------------------------------------------------
    # ---    Calculate idf values of each reference    ---
    reference_idf = metrics.get_idf(article_references, articles_to_cluster)

    # ----------------------------------------------------
    # - Associate version_id with an ordered_id [0..N-1] -
    ordered_version_id = {}
    version_ordered_id = {}
    for o_id, v_id in enumerate(articles_to_cluster):
        ordered_version_id[o_id] = v_id
        version_ordered_id[v_id] = o_id

    # ----------------------------------------------------
    # ---  Create a graph with all nodes but no links  ---
    g = igraph.Graph(directed=False)
    g.add_vertices(len(articles_to_cluster))

    for v in g.vs:
        # print articles[ordered_version_id[v.index]]["authors"]
        v["article_id"] = ordered_version_id[v.index]

    # ----------------------------------------------------
    # ---           Add links to the graph:            ---
    edges = set()
    weight_concept = {}
    weight_reference = {}

    concept_vector = {}
    reference_vector = {}
    for article_id in articles_to_cluster:
        concept_vector[article_id] = metrics.get_normed_vector(
            article_concepts.get(article_id, dict()),
            concept_idf,
            article_tfs=article_tfs[article_id]["concepts"] if article_tfs else dict())
        reference_vector[article_id] = metrics.get_normed_vector(
            article_references.get(article_id, dict()),
            reference_idf)
        if new_article in reference_vector.get(article_id, dict()):
            reference_vector[article_id].pop(new_article)
            metrics.normalize(reference_vector[article_id])
        if not any(concept_vector[article_id]) and not any(reference_vector[article_id]):
            concept_vector[article_id] = reference_vector[article_id] = metrics.get_normed_vector(article_tfs[article_id]["concepts"])

    for (v1_id, v2_id) in itertools.combinations(articles_to_cluster, r=2):
        edge = (version_ordered_id[v1_id], version_ordered_id[v2_id])
        similarity = metrics.get_similarity(
            concept_vector.get(v1_id, dict()),
            concept_vector.get(v2_id, dict()))
        if similarity != 0:
            edges.add(edge)
            weight_concept[edge] = similarity
        similarity = metrics.get_similarity(
            reference_vector.get(v1_id, dict()),
            reference_vector.get(v2_id, dict()))
        if similarity != 0:
            edges.add(edge)
            weight_reference[edge] = similarity

    list_edges = []
    edge_weight = {}
    edge_reference_weight = {}

    if not any(article_concepts) and not any(article_references): # aggregation_scheme == "aggregated":
        sum_weights_concept = 1.
        sum_weights_reference = 1.
    else:
        sum_weights_concept = sum(weight_concept.values()) if any(weight_concept) else 1.
        sum_weights_reference = sum(weight_reference.values()) if any(weight_reference) else 1.
    for edge in edges:
        weight = (
            weight_concept.get(edge, 0) * 1. / sum_weights_concept +
            weight_reference.get(edge, 0) * 1. / sum_weights_reference
        )
        if weight != 0:
            list_edges.append(edge)
            edge_weight[edge] = weight
            edge_reference_weight[edge] = weight_reference.get(edge, 0) * 1. / sum_weights_reference

    g.add_edges(list_edges)
    for edge in g.es:
        edge["weight"] = edge_weight[edge.tuple]
        edge["reference_weight"] = edge_reference_weight[edge.tuple]

    return g

