import math
from collections import defaultdict, Counter
from timeit import default_timer as timer
import sys


def get_idf(nodes, cluster_nodes, new_article=None):
    df = Counter()
    number_nodes_to_cluster = len(cluster_nodes)
    for article_id in cluster_nodes:
        df.update(nodes.get(article_id))
    idf = { # equal to zero in the case of 1 node to cluster
      c: math.log10(1.0 * number_nodes_to_cluster / df)
      for c, df in df.iteritems()
    }
    return idf


def get_average_author_vector(author_articles, articles_vectors):
    author_vector = Counter()
    for article_id in author_articles:
        author_vector.update(articles_vectors[article_id])
    normalize(author_vector)
    return author_vector


def get_average_cluster_vector(articles, cluster_idfs, cluster_parent, cluster_nodes, subclusters, article_concepts, new_article=None, weighting_scheme="tfidf", min_number_nodes_to_cluster=2):
    cluster_vector = {}
    invalid_clusters = {}
    articles_vectors = {}
    for cluster_id in subclusters:
        # TODO: check if exclusion of new_article is needed
        number_articles_to_cluster = len(cluster_nodes[cluster_id])
        # if number_articles_to_cluster < min_number_nodes_to_cluster:
            # print '{}\tarticles lack {}'.format(cluster_id, len(cluster_nodes[cluster_id]))
            # invalid_clusters[cluster_id] = cluster_nodes[cluster_id]
            # continue
        cluster_vector[cluster_id] = Counter()

        for article_id in cluster_nodes[cluster_id]:
            # sys.stdout.write(str(cluster_parent[cluster_id]))
            article_vector = get_normed_vector(
                article_concepts[article_id],
                cluster_idfs[cluster_parent],
                articles[article_id]["concepts"] if weighting_scheme == "tfidf" else dict()
                )
            articles_vectors[article_id] = article_vector
            if article_id != new_article:
                cluster_vector[cluster_id].update(article_vector)
        if not any(cluster_vector[cluster_id]):
            # TODO: return the list of rejected articles
            # print "{}\tconcepts lack {}".format(cluster_id, len(cluster_nodes[cluster_id]))
            invalid_clusters[cluster_id] = cluster_nodes[cluster_id]
            del cluster_vector[cluster_id]
            continue
        normalize(cluster_vector[cluster_id])
    return cluster_vector, set(invalid_clusters), articles_vectors


def get_normed_vector(article_components, cluster_idfs=dict(), article_tfs=dict()):
    article_vector = {
      c: cluster_idfs.get(c, 1.) * article_tfs.get(c,  1.)
      for c in article_components
    }
    normalize(article_vector)
    return article_vector


def normalize(vector, copy=False):
    norm = get_norm(vector)
    if norm > 0:
        if copy:    # Copy original vector, then modify the copy
            vector = {}
            for c in vector:
                vector[c] = vector[c] / norm
        else:   # Modify original vector
            for c in vector:
                vector[c] /= norm
    return vector


def get_norm(vector, sample=None):
    norm_square = 0
    for c in sample if sample else vector:
        norm_square += math.pow(vector[c], 2)
    return math.sqrt(norm_square)


def get_similarity(v1, v2):
    # Concept based cos similarity of normalized vectors
    common_components = set(v1) & set(v2)
    similarity = 0
    for c in common_components:
        similarity += v1[c] * v2[c]
    return similarity

